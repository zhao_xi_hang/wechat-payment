<?php

namespace WechatPayment\interfaces;

interface IWechatPaymentScore
{
    function run($data);
}