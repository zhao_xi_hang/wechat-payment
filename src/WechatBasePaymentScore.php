<?php


namespace WechatPayment;


use WechatPayment\tools\RandomStr;

class WechatBasePaymentScore
{
    private static $instance;

    /**
     * @param $config array 开启微信支付分所需要的配置参数
     * @param string $operation
     * @return mixed
     */
    static function getInstance($config,$operation = 'create_order'){
        if(empty($config) || empty($operation)){
            throw new \RuntimeException('WechatPayment参数有误:', 40004);
        }
        if (empty(self::$instance[$operation])) {
            $random_str = new RandomStr();
            $operation = $random_str->parse_name($operation, 1);
            $class = __NAMESPACE__  . "\\lib\\".$operation;
            if (class_exists($class)) {
                self::$instance[$operation] = new $class($config);
            } else {
                throw new \RuntimeException('WechatPayment for operation:', 40004);
            }
        }

        return self::$instance[$operation];
    }


}