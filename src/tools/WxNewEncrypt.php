<?php


namespace WechatPayment\tools;


class WxNewEncrypt
{
    private $app_key='';

    const WX_PAY_ERROR = 100 ;
    const WX_SIGN_ERROR = 101 ;

    public function __construct($config){
        $this->app_key = $config['app_key'];
    }

    protected $values = array();


    /**
     * 设置签名，详见签名生成算法
     * @param string $value
     * @return string 值
     **/
    public function SetSign($arr)
    {
        $this->values = $arr;
        $sign = $this->MakeSign();
        $this->values['sign'] = $sign;
        return $sign;
    }

    /**
     * 获取签名，详见签名生成算法的值
     * @return string 值
     **/
    public function GetSign()
    {
        return $this->values['sign'];
    }

    /**
     * 判断签名，详见签名生成算法是否存在
     * @return true 或 false
     **/
    public function IsSignSet()
    {
        return array_key_exists('sign', $this->values);
    }



    /**
     * 格式化参数格式化成url参数
     */
    public function ToUrlParams()
    {
        $buff = "";
        foreach ($this->values as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * 生成签名
     * @return string 签名，本函数不覆盖sign成员变量，如要设置签名需要调用SetSign方法赋值
     */
    public function MakeSign()
    {
        //签名步骤一：按字典序排序参数
        ksort($this->values);
        $string = $this->ToUrlParams();
        //签名步骤二：在string后加入KEY
        $string = $string . "&key=".$this->app_key;
        //签名步骤三：HMAC-SHA256
        $string = hash_hmac("sha256",$string ,$this->app_key);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    /**
     * 解密 xml=>json=>array
     * @param $params
     * @return mixed
     */
    public function decrypt($params){
        return $this->CheckSign($params);
    }

    /**
     * 检测签名
     * @param $arr
     * @return array|bool
     */
    public function CheckSign($arr)
    {
        if(!$this->IsSignSet()){
            throw new \RuntimeException('sign illegal',self::WX_SIGN_ERROR);
        }
        $xmlSign = "";
        foreach( $arr as $k=>$v) {
            if($k == 'sign') {
                $xmlSign = $arr[$k];//取出sign
                unset($arr[$k]);
            }
        }
        $sign = $this->MakeSign();
        if($xmlSign == $sign){
            //签名正确
            return true;
        }
        throw new \RuntimeException('sign illegal'. "$sign <<==>> $xmlSign",self::WX_SIGN_ERROR);
    }
}