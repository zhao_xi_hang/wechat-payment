<?php


namespace WechatPayment\lib;

/**
 * 查询支付分订单
 * Class QueryOrder
 * @package WechatPayment
 */
class QueryOrder extends WechatPaymentScore
{

    public $method = 'GET';

    /**
     * 组装参数
     * @param $data
     * @return mixed
     */
    protected function assembly($data)
    {
        return $data;
    }

    public function getUrl()
    {
        return $this->serverUrl;
    }
}