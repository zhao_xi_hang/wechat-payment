<?php


namespace WechatPayment\lib;

/**
 * 取消订单
 * Class CancelOrder
 * @package WechatPayment
 */
class CancelOrder extends WechatPaymentScore
{
    protected $method = 'POST';

    protected $out_order_no ;

    public function assembly($resource){
        $this->out_order_no = $resource['out_order_no'];
        unset($resource['out_order_no']);
        return $resource;
    }

    public function getUrl()
    {
        return $serverUrl = $this->serverUrl . '/'.$this->out_order_no.'/cancel';
    }
}