<?php


namespace WechatPayment\lib;

/**
 * 完结支付分订单
 * Class FinishOrder
 * @package WechatPayment
 */
class FinishOrder extends WechatPaymentScore
{
    public $method = 'POST';

    protected $out_order_no ;


    public function assembly($resource){
        $this->out_order_no = $resource['out_order_no'];
        unset($resource['out_order_no']);
        $resource['post_payments'][0]['name'] = '服务费';
        if(isset($resource['post_payments']) && isset($resource['total_amount']) && !empty($resource['post_payments']) && !empty($resource['total_amount'])){

        }else{
            $resource['post_payments'][0]['amount'] = 0;
            $resource['total_amount'] = 0;
        }
        $resource['time_range']['end_time'] = date('YmdHis',$this->getTime());
        return $resource;
    }

    public function getUrl()
    {
        return $serverUrl = $this->serverUrl . '/'.$this->out_order_no.'/complete';
    }
}