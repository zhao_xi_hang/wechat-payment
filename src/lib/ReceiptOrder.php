<?php


namespace WechatPayment\lib;

/**
 * 支付分订单收款
 * Class ReceiptOrder
 * @package WechatPayment
 */
class ReceiptOrder extends WechatPaymentScore
{
    public $method = 'POST';

    protected $out_order_no ;

    /**
     * 组装参数
     * @param $data
     * @return mixed
     */
    protected function assembly($data)
    {
        $this->out_order_no = $data['out_order_no'];
        unset($data['out_order_no']);
        return $data;
    }

    public function getUrl()
    {
        return $serverUrl = $this->serverUrl . '/'.$this->out_order_no.'/pay';

    }
}