<?php


namespace WechatPayment\lib;

/**
 * 创建订单
 * Class CreateOrder
 * @package WechatPayment
 */
class CreateOrder extends WechatPaymentScore
{

    protected $method = 'POST';
    /**
     * 组装参数
     * @param $resource
     * @return mixed
     */
    protected function assembly($resource)
    {
        //公众账号ID
        $resource['appid'] = $this->getConfig('appid');
        //服务ID
        $resource['service_id'] = $this->getConfig('service_id');
        #---服务时间段---
        //服务开始时间，单位秒
        $resource['time_range']['start_time'] = date('YmdHis', strtotime("+5 minutes"));
        //商户回调地址
        $resource['notify_url'] = $this->notify_url;
        //是否需要用户确认
        $resource['need_user_confirm'] = isset($resource['need_user_confirm'])?$resource['need_user_confirm']:true;
        return $resource;
    }

    public function getUrl()
    {
        return $this->serverUrl;
    }
}