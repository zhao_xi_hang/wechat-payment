<?php


namespace WechatPayment\lib;


use WechatPayment\interfaces\IWechatPaymentScore;
use WechatPayment\tools\Encryption;
use WechatPayment\tools\RandomStr;
use WechatPayment\tools\Request;

abstract class WechatPaymentScore implements IWechatPaymentScore
{
    protected $_config = [];

    //调用方法
    protected $method;

    protected $time;

    //创建订单回调地址
    protected  $notify_url ;

    //创建支付分订单接口url(查询同样)
    protected $serverUrl = 'https://api.mch.weixin.qq.com/v3/payscore/serviceorder';

    /**
     * WechatPayment constructor.
     * @param $config array
     */
    public function __construct($config)
    {
        $this->setConfig($config);

        $this->notify_url = str_replace("http://","https://",$config['notify_url']);
    }

    private function setConfig($config){
        $this->_config['appid'] = $config['appid'];
        $this->_config['mch_id'] = $config['mch_id'];
        $this->_config['key'] = $config['key'];
        $this->_config['api_key'] = $config['api_key'];
        $this->_config['service_id'] = $config['service_id'];
        $this->_config['service_no'] = $config['service_no'];
        $this->_config['apiclient_key'] = $config['apiclient_key'];
    }

    protected function getConfig($key){
        return $this->_config[$key];
    }

    protected function setTime(){
        return $this->time = time();
    }

    protected function getTime(){
        return $this->time;
    }

    protected function getMethod(){
        return $this->method;
    }

    public function run($data)
    {
        $this->setTime();
        $data = $this->assembly($data);
        $url = $this->getUrl();
        $data['service_id'] = $this->getConfig('service_id');
        $data['appid'] = $this->getConfig('appid');
        $sign = $this->doEncryption($data,$url);
        $return_value = $this->send($sign,$url,$data,$this->getMethod());
        $this->returnValue($return_value);
        return $return_value;
    }

    /**
     * 加密
     * @param $data
     * @param $method
     * @param $url
     * @param $needpath
     * @return string
     */
    protected function doEncryption($data,$url){
        $path = $this->RemoverDomainName($url);
        //获取签名
        $array = ['service_no'=>$this->getConfig('service_no'),'mch_id'=>$this->getConfig('mch_id'),'apiclient_key' => $this->getConfig('apiclient_key')];
        $rand = new RandomStr();
        $Encryption = new Encryption($array);
        $sign = $Encryption->get_signature($path, $this->getMethod(), $data,strtoupper($rand->randomStr(32)),$this->getTime());
        return $sign;
    }

    /**
     * 发送请求
     * @param $sign
     * @param $url
     * @param $params
     * @param string $method
     * @param string $needpath
     * @return array|mixed
     */
    protected function send($sign,$url,$params,$method='POST'){
        $Authorization = 'Authorization: WECHATPAY2-SHA256-RSA2048 ' . $sign;
        $request = new Request();
        $return_value = $request->curl_request($url, $method, $params, true, $Authorization);
        return $return_value;
    }

    function returnValue(&$return_value){
        return $return_value;
    }

    /**
     * 去除字符串中的域名
     * @param $url
     * @return mixed
     */
    private function RemoverDomainName($url){
        $array = parse_url($url);
        if(!is_array($array)){
            return ['status'=>false,'msg'=>'传入url不符合格式'];
        }
        $url = $array['path'];
        return $url;
    }

    /**
     * 组装参数
     * @param $data
     * @return mixed
     */
    abstract protected function assembly($data);

    //生成url
    abstract public function getUrl();
}