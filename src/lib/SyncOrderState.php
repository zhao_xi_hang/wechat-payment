<?php


namespace WechatPayment\lib;

/**
 * 同步订单服务状态
 * Class SyncOrderState
 * @package WechatPayment
 */
class SyncOrderState extends WechatPaymentScore
{
    public $method = 'POST';
    public $out_order_no ;
    public function getUrl()
    {
        return $serverUrl = $this->serverUrl . '/'.$this->out_order_no.'/sync';
    }
    public function assembly($resource){
        $this->out_order_no = $resource['out_order_no'];
        unset($resource['out_order_no']);
        return $resource;
    }
}