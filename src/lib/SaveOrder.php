<?php


namespace WechatPayment\lib;

/**
 * 修改订单金额
 * Class SaveOrder
 * @package WechatPayment
 */
class SaveOrder extends WechatPaymentScore
{
    public $method = 'POST';
    public $out_order_no ;

    public function getUrl()
    {
        return $serverUrl = $this->serverUrl . '/'.$this->out_order_no.'/modify';
    }

    public function assembly($resource){
        $this->out_order_no = $resource['out_order_no'];
        unset($resource['out_order_no']);
        return $resource;
    }
}