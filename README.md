# 微信支付分

#### 功能
调用微信支付分的一个composer包

#### 实例化
WechatBasePaymentScore::getInstance($config,$operation)
第二个参数根据你需要创建的对象去传递

创建支付分订单 $operation 可不传 或传 'CreateOrder'|'create_order'

查询支付分订单 $operation = 'QueryOrder'|'query_order'

取消支付分订单 $operation = 'CancelOrder'|'cancel_order'

修改订单金额 $operation = 'SaveOrder'|'save_order'

完结支付分订单 $operation = 'FinishOrder'|'finish_order'

商户发起催收扣款 $operation = 'ReceiptOrder'|'receipt_order'

同步服务订单信息 $operation = 'SyncOrderState'|'sync_order_state'
#### 参数说明

$config 中包含
````
$config['appid'];   小程序appid
$config['mch_id'];  商户id
$config['key'];     商户key
$config['api_key']; api_key
$config['service_id']; 服务id
$config['service_no']; 服务no
$config['apiclient_key']; 小程序key的路径
````
调用run方法传递的参数请参考
`https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter6_1_14.shtml`

