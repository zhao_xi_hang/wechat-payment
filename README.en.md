# 微信支付分

#### 功能
调用微信支付分的一个composer包

#### 实例化
WechatBasePaymentScore::getInstance($config,'create_order')
第二个参数根据你需要创建的对象去传递
#### 参数说明

$config 中包含
````
$config['appid'];   小程序appid
$config['mch_id'];  商户id
$config['key'];     商户key
$config['api_key']; api_key
$config['service_id']; 服务id
$config['service_no']; 服务no
$config['apiclient_key']; 小程序key的路径
````


